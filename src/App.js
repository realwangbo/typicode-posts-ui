import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Collapsible from 'react-collapsible';
import './App.css';

function App() {
  const [posts, setPosts] = useState(null);

  useEffect(() => {
    const getPosts = async () => {
      try {
        const res = await axios.get(
          `http://localhost:5004/typicode-api/posts`,
        );
        setPosts(res.data);
      } catch (e) {
        console.log(e);
      }
    };
    getPosts();
  }, []);

  return (
    <div className="App">
      <h3>Posts Data From https://jsonplaceholder.typicode.com/</h3>
      <label>You can click the post titles to read content and comments.</label>
      <div className="PostsContainer">
        {posts && posts.map((post, index)=> (
          <div className="PostItem" key={index}>
            <Collapsible trigger={"Post " + post.id + ": " + post.title}>
              <p>{post.body}</p>
              <p>
                <div className="CommentList">
                  {post.comments && (post.comments.map((comment, i) => (
                    <div className="CommentItem" key={i}>
                      <div className="commentHeader">
                        <span id="commentFrom">From: {comment.email}</span>
                        <span>Comment name: {comment.name}</span>
                      </div>
                      <label>{comment.body}</label>
                    </div>
                  )))}
                </div>
              </p>     
            </Collapsible>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
