# Typicode posts ui

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements

```sh-session
Node >= 10.16 and npm >= 5.6 (https://nodejs.org/en/download/)
```

## Steps to run

```sh-session
1. Make sure the back-end API is running on: http://localhost:5004
2. $ npm install
3. $ npm start
4. The app will automatically run in your browser. If not, open http://localhost:3000/
```

## Notes
```sh-session
If you get this error: "SyntaxError: Unexpected token {" when run "npm start", that's because you are running on a lower version of node. Install the latest version of node will fix it.
```

